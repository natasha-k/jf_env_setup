"""Top-level package for jf_env_setup."""

__author__ = """Natasha Kelkar"""
__email__ = "natasha.kelkar@jellyfishpictures.co.uk"
__version__ = "0.1.0"
