"""Console script for jf_env_setup."""
import argparse
import sys

from jf_env_setup.jf_env_setup import write_yaml


def main():
    """Console script for test_test."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--project', help="Project name", required=True)
    parser.add_argument('--config', help="Tank config name", required=True)
    parser.add_argument('--sgtk_root', help="Sgtk config root", required=True)
    parser.add_argument('--container', help="Config name", default="mull")
    parser.add_argument('--project_root', help="Mapped drive letter for project", default="P:")
    parser.add_argument('--department_root', help="Mapped drive letter for dept", default="Q:")
    args = parser.parse_args()
    filename = write_yaml(
        args.project, args.config, args.container, args.sgtk_root, args.project_root, args.department_root
    )
    print(filename)
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
