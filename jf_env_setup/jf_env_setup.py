"""Main module."""
import os
import yaml

from jinja2 import Environment, FileSystemLoader

template_path = os.path.join(os.path.dirname(__file__), "templates")
env = Environment(loader=FileSystemLoader(template_path))


def write_yaml(project, config, container, sgtk_root, project_root, department_root):
    env_vars = {
        "JF_PROJECT": project,
        "JF_PROJECT_TANKNAME": project,
        "JF_CONTAINER": container,
        "JF_PROJECTS_ROOT": project_root,
        "JF_DEPARTMENTAL_ROOT": department_root,
        "JF_TANK_CONFIG_NAME": config,
        "JF_SGTK_CONFIG_ROOT": sgtk_root,
        "JF_PYTHON_MAJOR": "2",
        "JF_PYTHON_MINOR": "7",
        "JF_PYTHON_PATCH": "14",
        "JF_PYTHON3_MAJOR": "3",
        "JF_PYTHON3_MINOR": "8",
        "JF_PYTHON3_PATCH": "1",
        "USERNAME": os.environ["USERNAME"]
    }
    ordered_templates = [
        "jf_studio.j2",
        "jf_project.j2",
        "jf_user.j2"
    ]
    if "dev" in config:
        ordered_templates.append("jf_dev.j2")

    for each in ordered_templates:
        if each == "jf_user.j2":
            env_vars['JF_TOOLKIT'] = "C:\\local_pipe\\projects\\{}\\pipeline\\jfToolkit".format(project)
        elif each == "jf_dev.j2":
            env_vars['JF_TOOLKIT'] = "C:\\jfpdk\\jfToolkit"
        template = env.get_template(each)
        new_vars = template.render(env_vars)
        env_vars.update(yaml.safe_load(new_vars))

    filename = os.path.join(os.environ['TEMP'], "{}_env_setup.yaml".format(env_vars['JF_PROJECT']))
    with open(filename, "w") as f:
        yaml.dump(env_vars, f)
    return filename
