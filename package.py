name = "jf_env_setup"

version = "0.1.0"

authors = [
    "Natasha Kelkar"
]

description = \
    """
    Builds the relevant JF environment paths required for setup using templates
    """

requires = [
    "python",
    "PyYAML"
]

build_command = "python {root}/rezbuild.py {install}"


def commands():
    env.PYTHONPATH.append("{root}/python")
    env.PATH.append("{root}/bin")


tests = {
    "unit": {
        "command": "python setup.py test",
        "requires": ["python"],
    },
    "lint": {
        "command": "pylint jf_env_setup",
        "requires": ["pylint"]
    },
}