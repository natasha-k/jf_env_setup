#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("CHANGELOG.md") as history_file:
    history = history_file.read()

test_requirements = []

setup(
    author="Natasha Kelkar",
    author_email="natasha.kelkar@jellyfishpictures.co.uk",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7"
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    description="Builds the relevant JF environment paths required for setup using templates",
    entry_points={
        'console_scripts': [
            'jf_env_setup=jf_env_setup.cli:main',
        ],
    },
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="jf_env_setup",
    name="jf_env_setup",
    packages=find_packages(include=["jf_env_setup", "jf_env_setup.*"]),
    data_files=[("Lib/site-packages/jf_env_setup/templates",
                 ["jf_env_setup/templates/jf_studio.j2",
                  "jf_env_setup/templates/jf_project.j2",
                  "jf_env_setup/templates/jf_user.j2",
                  "jf_env_setup/templates/jf_dev.j2"])],
    test_suite="tests",
    url="https://bitbucket.org/jf-pictures/jf_env_setup",
    version="0.1.0",
    zip_safe=False,
)
